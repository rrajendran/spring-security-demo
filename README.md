spring-security-demo
====================
Show cases simple spring security project.


How to run
==========
mvn clean jetty:run

1. Navigate to <a href="http://localhost:8080/spring-security-demo/secure/index" target="_blank">spring-security-demo</a>
2. Credentials: ramesh/welcome1
